# -*- coding: utf-8 -*-

import sys
# import vispa
import os
import json
import logging
from subprocess import Popen, PIPE
from vispa.remote import AjaxException
import datetime
import subprocess
import copy

# import graphviz as gv
import xml.etree.ElementTree as ET

from AnalysisWorkflowManagement.core.Workflow import Workflow
from AnalysisWorkflowManagement.tools.OutputRedirector import OutputRedirector

logger = logging.getLogger(__name__)

_globalOutputRedirector = OutputRedirector()
# sys.path.append("../software/ttbb_site-packages") # path to site-packages

print datetime.datetime.now()

class Digraph(object):
    def __init__(self, name=None, format=None, subgraphs=None):
        self.name = name
        self.format = format if format else "svg"
        self.source = "digraph {\n}"
        self.subgraphs = subgraphs if subgraphs else {}
        self.append("label=" + self.name)

    def subgraph(self, graph):
        self.subgraphs[graph.name] = graph

    def append(self, text):
        self.source = self.source[:-1] + text + ";\n}"

    def edge(self, a, b, label=None, id=None):
        edge = a + " -> " + b + " ["
        if label:
            edge += 'label="' + label + '",'
        if id:
            edge += 'id="' + id + '",'
        edge += "]"
        self.append(edge)

    def node(self, node, label=None, id=None):
        node += " ["
        if label:
            node += 'label="' + label + '",'
        if id:
            node += 'id="' + id + '",'
        node += "]"
        self.append(node)

    def merge(self):
        for subgraph in self.subgraphs:
            self.subgraphs[subgraph].merge()
            self.append("subgraph "+ self.subgraphs[subgraph].name + self.subgraphs[subgraph].source[7:])

class Step(object):
    def __init__(self, name):
        self.name = name
        self.x = None
        self.y = None
        self.width = None
        self.height = None
        # self.creatorSteps = []
        self.childSteps = []
        # self.dataCollections = []
        self.requiredDataCollections = {}

    def toDict(self):
        return self.__dict__


class Cluster(object):

    def __init__(self, name=None, graph=None, parent=None, daughters=None, steps=None):
        self.name = name
        self.graph = graph
        self.parent = parent
        self.daughters = daughters if daughters else {}
        self.steps = steps if steps else {}
        self.collapsedFlag = False

    def __str__(self):
        return "Cluster(%s, %s, %d daughters)" % (self.name,
                                                  hex(id(self)),
                                                  len(self.daughters))

    def appendDaughter(self, node):
        self.daughters[node.name] = node
        node.parent = self
        return node

    def appendStep(self, step):
        self.steps[step.name] = step
        return step

    def getClusterClusters(self):
        tpl = (self.name,)
        if self.parent:
            return self.parent.getClusterClusters() + tpl
        return tpl

    def getCluster(self, clusters):
        """
            clusters = (mu, nominal, bdt)
        """
        lead = clusters[0]
        remainder = clusters[1:]

        try:
            daughter = self.daughters[lead]
        except KeyError:
            return None

        # daughter = self.daughters.get(lead, None)
        # if not daughter:
        #    return None

        if daughter.name == lead:
            if len(remainder) > 0:
                return daughter.getCluster(remainder)
            else:
                return daughter
        return None

    def merge(self):
        for daughterName in self.daughters.keys()[:]:
            daughter = self.daughters[daughterName]
            daughter.merge()

            self.graph.subgraph(daughter.graph)

            del self.daughters[daughterName]

        return self

    def mergeGraphs(self):
        for daughter in self.daughters:
            self.daughters[daughter].mergeGraphs()
            self.graph.append("subgraph " + self.daughters[daughter].graph.name + self.daughters[daughter].graph.source[7:])

    def createRecursively(self, clusters):
        # Added try-except
        try:
            lead = clusters[0]
            remainder = clusters[1:]
        except IndexError:
            return self
            lead = ""
            remainder = ""

        try:
            daughter = self.daughters[lead]
        except KeyError:
            # daughter = self.appendDaughter(Cluster(name=lead, graph=Digraph(name="cluster_" + str(clusters))))
            graphName = "".join(self.getClusterClusters()) + "".join((lead,))
            daughter = self.appendDaughter(Cluster(name=lead, graph=Digraph(format=fileformat, name="cluster_" + str(graphName))))

        if len(remainder) > 0:
            return self.daughters[lead].createRecursively(remainder)

        return daughter

    def toDict(self):
        attributes = ['name', 'collapsedFlag']
        d = dict((attr, getattr(self, attr)) for attr in attributes)
        d['daughters'] = dict((daughter.name, daughter.toDict()) for daughter in self.daughters.values())
        d['steps'] = dict((step.name, step.toDict()) for step in self.steps.values())

        return d

    def getJSON(self):
        return json.dumps(self.toDict(), indent=4)

def getCommonClusters(clusters1, clusters2):
    commonClusterIndex = 0
    if (len(clusters1) > 0 and len(clusters2) > 0):
        for i in range(min(len(clusters1), len(clusters2))):
            if (clusters1[i] == clusters2[i]):
                commonClusterIndex = i+1
            else:
                break
        return clusters1[0:commonClusterIndex]
    else:
        return ()

fileformat = "svg"
def getWorkflow(path, rankdir, ratio):
    try:
        workflow = Workflow.loadFromFile(
            workflowPythonFilename=path,
            silentFlag=False)
    except Exception as e:
        logger.debug(str(e))

    steps = workflow.getAnalysisSteps()
    detailLevel = 100             # Has to be greater than 0
    tooDetailed = {}
    discardedClusters = []
    nodeIDs = []
    clusterIDs = []
    styleOfClusters = "node [shape = rectangle, peripheries = 2, fillcolor=gray, style = filled]"
    styleOfSteps = "node [shape = ellipse, peripheries = 1, fillcolor=white, style = filled]"
    styleOfClusters = styleOfSteps

    root = Cluster(name="root", graph=Digraph(name="root", format=fileformat), parent=None)
    root.graph.append('ratio=' + ratio)
    root.graph.append('rankdir=' + rankdir)

    for step in steps:
        clusters = step.getClusters()
        if len(clusters) > detailLevel:
            discardedCluster = clusters[detailLevel]
            if discardedCluster not in discardedClusters:
                discardedClusters.append(discardedCluster)
            clusters = clusters[0:detailLevel]
            print "Shortened Clusters:", clusters
            tooDetailed[step.getName()] = clusters + (discardedCluster, )
            node = root.createRecursively(clusters)
            if not hasattr(node, "styled") or not node.styled:
                node.styled = True
                root.createRecursively(clusters).graph.append('label = " '+  "_".join(clusters) + '"')
                root.createRecursively(clusters).graph.append(styleOfClusters)
                clusterID = "cluster_" + discardedCluster
                root.createRecursively(clusters).graph.node("Cluster\n" + discardedCluster, id=clusterID)
                if clusterID not in clusterIDs:
                    clusterIDs.append(clusterID)
        else:
            node = root.createRecursively(clusters)
            if not hasattr(node, "styled") or not node.styled:
                node.styled = True
                root.createRecursively(clusters).graph.append(styleOfSteps)
                root.createRecursively(clusters).graph.append('label = " '+  "_".join(clusters) + '"')
            nodeID = step.getName()
            root.createRecursively(clusters).graph.node(step.getName(), id=nodeID)
            root.createRecursively(clusters).appendStep(Step(step.getName()))
            if nodeID not in nodeIDs:
                nodeIDs.append(nodeID)

    seen = []

    for step in steps:
        dcrList = step.getRequiredDataCollections()
        for dcr in dcrList:
            stepClusters = step.getClusters()
            stepName = step.getName()
            creatorClusters = workflow.getAnalysisStep(dcr.creatorName).getClusters()
            creatorName = dcr.creatorName
            # root.createRecursively(stepClusters).steps[stepName].requiredDataCollections.append({'creatorName': dcr.creatorName, 'dataCollectionName': dcr.dataCollectionName, 'label': dcr.label, 'usage': dcr.usage, 'allFlag': dcr.allFlag, 'mandatoryFlag': dcr.mandatoryFlag})
            try:
                root.createRecursively(stepClusters).steps[stepName].requiredDataCollections[dcr.label].append({'creatorName': dcr.creatorName, 'dataCollectionName': dcr.dataCollectionName, 'usage': dcr.usage, 'allFlag': dcr.allFlag, 'mandatoryFlag': dcr.mandatoryFlag})
            except KeyError:
                root.createRecursively(stepClusters).steps[stepName].requiredDataCollections[dcr.label] = []
                root.createRecursively(stepClusters).steps[stepName].requiredDataCollections[dcr.label].append({'creatorName': dcr.creatorName, 'dataCollectionName': dcr.dataCollectionName, 'usage': dcr.usage, 'allFlag': dcr.allFlag, 'mandatoryFlag': dcr.mandatoryFlag})
            # if dcr.dataCollectionName not in root.createRecursively(stepClusters).steps[stepName].dataCollections:
            #     root.createRecursively(stepClusters).steps[stepName].dataCollections.append(dcr.dataCollectionName)
            if step.getName() in tooDetailed:
                stepClusters = tooDetailed[step.getName()][0:detailLevel]
                stepName = "Cluster\n" + tooDetailed[step.getName()][detailLevel]
            if dcr.creatorName in tooDetailed:
                creatorClusters = tooDetailed[dcr.creatorName][0:detailLevel]
                creatorName = "Cluster\n" + tooDetailed[dcr.creatorName][detailLevel]
            commonClusters = getCommonClusters(stepClusters, creatorClusters)
            # root.createRecursively(commonClusters).graph.append(styleOfSteps)
            if step.getName() in tooDetailed or dcr.creatorName in tooDetailed:
                root.createRecursively(commonClusters).graph.append(styleOfClusters)
            if len(commonClusters) > 0:
                pass
                # print "Common Cluster of", stepName, "and", creatorName, ":", commonClusters
            # if dcr.creatorName not in root.createRecursively(stepClusters).steps[stepName].creatorSteps:
            #     root.createRecursively(stepClusters).steps[stepName].creatorSteps.append(dcr.creatorName)
            if stepName not in root.createRecursively(creatorClusters).steps[dcr.creatorName].childSteps:
                root.createRecursively(creatorClusters).steps[dcr.creatorName].childSteps.append(stepName)
            if (creatorName, stepName) not in seen:
                root.createRecursively(commonClusters).graph.edge(creatorName, stepName, id=creatorName + stepName)
                seen.append((creatorName, stepName))

    return (root, workflow)


def getrelations():
    relations = {}
    for step in steps:
        relations[step.getName()] = {'creatorName': [],
                                     'children': [],
                                     'dataCollection': [],
                                     'clusters': step.getClusters()}
    for step in steps:
        dcrList = step.getRequiredDataCollections()
        for dcr in dcrList:
            if dcr.creatorName not in relations[step.getName()]['creatorName']:
                relations[step.getName()]['creatorName'].append(dcr.creatorName)
            if dcr.dataCollectionName not in relations[step.getName()]['dataCollection']:
                relations[step.getName()]['dataCollection'].append(dcr.dataCollectionName)
            if step.getName() not in relations[dcr.creatorName]['children']:
                relations[dcr.creatorName]['children'].append(step.getName())
    return relations

def getSVGfromCluster(cluster, engine):
    tempCluster = copy.deepcopy(cluster)
    tempCluster.mergeGraphs()
    cmd = engine + " -Tsvg"
    p = subprocess.Popen(cmd, stdin=subprocess.PIPE, stdout=subprocess.PIPE,
                         stderr=subprocess.PIPE, shell=True)
    stdout, stderr = p.communicate(tempCluster.graph.source)
    code = p.returncode
    logger.debug(tempCluster.graph.source)
    if code != 0:
        raise AjaxException(err)
    else:
        return stdout


class AWMRpc(object):

    def __init__(self):
        logger.debug("AWMRpc created")

    def get_relations(self):
        relations = getrelations()
        return json.dumps(relations)

    def get_cluster_tree(self):
        return json.dumps(getClusterTree())

    def read_file_content(self, filepath):
        try:
            filepath = os.path.expanduser(os.path.expandvars(filepath))
            with open(filepath, "r") as f:
                content = f.read()
            import json
            data = json.loads(content)

            return json.dumps(data)
        except Exception as e:
            raise AjaxException(str(e))

    def get_visualization(self):
        vis = getSVGfromCluster(root)
        return vis

    def get_coordinates(self):
        coords = {}
        svgString = getSVGfromCluster(root)
        dom = ET.fromstring(svgString)
        for g in dom.findall("*"):
            for h in g.findall("*"):
                try:
                    id = h.attrib['id']
                    try:
                        try:
                            cx = h.getchildren()[1].attrib['cx']
                            cy = h.getchildren()[1].attrib['cy']
                            rx = h.getchildren()[1].attrib['rx']
                            ry = h.getchildren()[1].attrib['ry']
                            xval = float(cx)-float(rx)
                            yval = float(cy)+float(ry)
                            heightval = 2*float(ry)
                            widthval = 2*float(rx)

                            coords[id] = {'x': xval, 'y': yval,
                                          'width': widthval,
                                          'height': heightval}
                        except (KeyError, IndexError):
                            try:
                                pass
                            except (KeyError, IndexError):
                                pass
                            else:
                                pass
                    except (KeyError, IndexError):
                        pass
                except (KeyError, IndexError):
                    pass
        return json.dumps(coords)

    def write_coordinates_to_root(self, root, workflow, engine):
        dom = ET.fromstring(getSVGfromCluster(root, engine))
        # logger.debug(getSVGfromCluster(root))
        for g in dom.findall("*"):
            for h in g.findall("*"):
                try:
                    id = h.attrib['id']
                    try:
                        try:
                            cx = h.getchildren()[1].attrib['cx']
                            cy = h.getchildren()[1].attrib['cy']
                            rx = h.getchildren()[1].attrib['rx']
                            ry = h.getchildren()[1].attrib['ry']
                            xval = float(cx)-float(rx)
                            yval = float(cy)+float(ry)
                            heightval = 2*float(ry)
                            widthval = 2*float(rx)

                            step = workflow.getAnalysisStep(id)
                            root.createRecursively(step.getClusters()).steps[step.getName()].x = xval
                            root.createRecursively(step.getClusters()).steps[step.getName()].y = yval
                            root.createRecursively(step.getClusters()).steps[step.getName()].height = heightval
                            root.createRecursively(step.getClusters()).steps[step.getName()].width = widthval

                        except (KeyError, IndexError):
                            try:
                                pass
                            except (KeyError, IndexError):
                                pass
                            else:
                                pass
                    except (KeyError, IndexError):
                        pass
                except (KeyError, IndexError):
                    pass

    def get_graph(self, filepath, engine, rankdir, ratio):
        filepath = os.path.expandvars(os.path.expanduser(filepath))
        root, workflow = getWorkflow(filepath, rankdir, ratio)
        self.write_coordinates_to_root(root, workflow, engine)
        # logger.debug(root.getJSON())
        return root.getJSON()

    def get_map(self, filepath, engine, rankdir, ratio):
        filepath = os.path.expandvars(os.path.expanduser(filepath))
        root, workflow = getWorkflow(filepath, rankdir, ratio)
        SVG = getSVGfromCluster(root, engine)
        dom = ET.fromstring(SVG)
        dom.set("height", "100%")
        dom.set("width", "100%")
        # dom.set("viewBox", "0 0 100 100 ")
        dom = ET.tostring(dom, method="xml")
        dom = dom.replace("ns0:", "")
        # logger.debug("################ dom:")
        # logger.debug(dom)
        return dom

print os.getcwd()
_globalOutputRedirector.restore()
