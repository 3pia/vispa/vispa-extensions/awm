# -*- coding: utf-8 -*-

from vispa.controller import AbstractController
from vispa.server import AbstractExtension

import cherrypy
import logging
import sys
import os
import json
# import graphviz as gv

# from AnalysisWorkflowManagement.core.Workflow import Workflow

class AWMExtension(AbstractExtension):

    def name(self):
        return "awm"

    def dependencies(self):
        return []

    def setup(self):
        self.add_controller(AWMController(self))
        self.add_workspace_directoy()

class AWMController(AbstractController):

    def __init__(self, extension):
        super(AWMController, self).__init__()

        self.extension = extension

    def getrpc(self):
        return self.get("proxy", "AWMRpc", self.get("combined_id"))

    @cherrypy.expose
    @cherrypy.tools.ajax(encoded=True)
    def filecontent(self, filepath):
        rpc = self.getrpc()
        data = rpc.read_file_content(filepath)
        return data

    @cherrypy.expose
    @cherrypy.tools.ajax(encoded=True)
    def getgraph(self, filepath, engine, rankdir, ratio):
        return self.getrpc().get_graph(filepath, engine, rankdir, ratio)

    @cherrypy.expose
    def getmap(self, filepath, engine, rankdir, ratio):
        return self.getrpc().get_map(filepath, engine, rankdir, ratio)
