define(function() {

    var defaultPreferences = {
        ratio: {
            descr: "The ratio of width and height of the Graph.",
            type: "float",
            value: 0.30,
            range: [0, 5, 0.01]
        },
        rankdir: {
            description: "The direction, the graph is plotted.",
            type: "string",
            value: "TB",
            selection: ["TB", "BT", "LR", "RL"]
        },
        engine: {
            description: "The engine, used to generate the graph.",
            type: "string",
            value: "dot",
            selection: ["dot", "twopi", "circo", "osage"]
        },
    };

    // var defaultShortcuts = {
    //   save: {
    //     description: "Save the input editor's content.",
    //     value: vispa.device.isMac ? "meta+s" : "ctrl+s",
    //     callback: function() {
    //       this.editor.actions.save();
    //     }
    //   },
    // };

    // var highlightModeEntries = [{
    //   id: "C/C++",
    //   callback: function() {
    //     this.editor.setSyntaxHighlighting("c");
    //   }
    // },
    // ];

    return {
        preferences: defaultPreferences,
        // shortcuts: defaultShortcuts,
        // highlightModeEntries: highlightModeEntries,
    }

});
