define([
    "vispa/extensions",
    "vispa/views/center",
    "jquery",
    "jclass",
    "./prefs",
    "bootstrap",
    "./vendor/jsPlumb-1.7.10.js",
    "css!../css/css",
    "vendor/fuelux/js/fuelux",
], function(Extensions, CenterView, $, jClass, Prefs) {

    var AWMExtension = Extensions.Extension._extend({

        init: function init() {
            init._super.call(this, "awm");

            var self = this;

            this.addView("center", AWMCenterView);

            this.addMenuEntry("Open Workflow", {
                iconClass: "fa fa-code-fork",
                callback: function(workspaceId) {
                    var fsExt = vispa.extensions.getExtension("file");
                    fsExt.createInstance(workspaceId, "FileSelector", {
                        callback: function(path) {
                            if (!path) return;
                            var ext = path.split(".").pop().toLowerCase();
                            if (ext != "py") {
                                vispa.messenger.alert("The selected file is not a python file. Please select a different one!");
                                fsExt.createInstance(workspaceId, callback);
                            } else {
                                console.log(path);
                                self.createInstance(workspaceId, AWMCenterView, { path: path });
                            }
                        }
                    });

                }
            });
          this.setDefaultPreferences(AWMCenterView, Prefs.preferences, {
            title: "Analysis Workflow Management"
          });

          this.setDefaultShortcuts(AWMCenterView, Prefs.shortcuts, {
            title: "Analysis Workflow Management"
          });
        }
    });
    var Step = jClass._extend({
        init: function(view) {
            this.name = name;
            this.creatorSteps = [];
            this.childSteps = [];
            this.requiredDataCollections = [];
            this.connectionPoint = "";
            this.id = "";
            this.highlightedFlag = false;
            this.view = view;
        },
        connect: function() {
            this.view.jsPlumb.detachAllConnections(this.name);
            var seen = []
            for (var i in this.childSteps) {
                if (this.view.stepDict[this.childSteps[i]].connectionPoint in seen) {
                } else {
                    this.view.jsPlumb.connect({
                        source: this.connectionPoint,
                        target: this.view.stepDict[this.childSteps[i]].connectionPoint
                    });
                    seen.push((this.connectionPoint, this.view.stepDict[this.childSteps[i]].connectionPoint));
                }
            };
            for (var i in this.creatorSteps) {
                if (this.view.stepDict[this.creatorSteps[i]].connectionPoint in seen) {
                } else {
                    this.view.jsPlumb.connect({
                        source: this.view.stepDict[this.creatorSteps[i]].connectionPoint,
                        target: this.connectionPoint
                    });
                    seen.push((this.view.stepDict[this.creatorSteps[i]].connectionPoint, this.connectionPoint));
                };
            };
        },
        show: function() {
            this.view.viewNode.find('#' + this.id).css({"display": "inline"});
        },
        hide: function() {
            this.view.viewNode.find('#' + this.id).css({"display": "none"});
        },
        highlightNearestFamily: function(flag) {
            if (flag) {
                this.view.viewNode.find('#' + this.id).removeClass("highlighted");
                this.highlightedFlag = false;
                for (var i in this.childSteps) {
                    $childStep = this.view.stepDict[this.childSteps[i]];
                    this.view.viewNode.find('#' + $childStep.id).removeClass("highlighted");
                    this.view.stepDict[this.childSteps[i]].highlightedFlag = false;
                };
                for (var i in this.creatorSteps) {
                    $creatorStep = this.view.stepDict[this.creatorSteps[i]];
                    this.view.viewNode.find('#' + $creatorStep.id).removeClass("highlighted");
                    this.view.stepDict[this.creatorSteps[i]].highlightedFlag = false;
                };
                this.view.jsPlumb.select({source: this.id}).setPaintStyle({strokeStyle: "black", lineWidth: 1,});
                this.view.jsPlumb.select({target: this.id}).setPaintStyle({strokeStyle: "black", lineWidth: 1,});
            } else {
                this.view.viewNode.find('#' + this.id).addClass("highlighted");
                this.highlightedFlag = true;
                for (var i in this.childSteps) {
                    $childStep = this.view.stepDict[this.childSteps[i]];
                    this.view.viewNode.find('#' + $childStep.id).addClass("highlighted");
                    this.view.stepDict[this.childSteps[i]].highlightedFlag = true;
                };
                for (var i in this.creatorSteps) {
                    $creatorStep = this.view.stepDict[this.creatorSteps[i]]
                    this.view.viewNode.find('#' + $creatorStep.id).addClass("highlighted");
                    this.view.stepDict[this.creatorSteps[i]].highlightedFlag = true;
                };
                this.view.jsPlumb.select({source: this.id}).setPaintStyle({strokeStyle: "red", lineWidth: 2,});
                this.view.jsPlumb.select({target: this.id}).setPaintStyle({strokeStyle: "red", lineWidth: 2,});
            }
        }
    });

    var Cluster = jClass._extend({
        init: function(view){
            this.name = "";
            this.collapsedFlag = false;
            this.daughters = {};
            this.steps = [];
            this.id = "";
            this.width = 0;
            this.height = 0;
            this.x = 0;
            this.y = 0;
            this.svgx = 0;
            this.svgy = 0;
            this.view = view;
        },
        print: function(){
            return "Cluster " + this.name
        },
        update_subcluster: function(visibleFlag, connPoint){
            // var seen = [];
            if (!visibleFlag) {
                this.view.viewNode.find("#" + this.id).css({"display": "none"});
                for (var step in this.steps) {
                    // console.log(this.steps[step].name);
                    $step = this.steps[step];
                    this.view.jsPlumb.detachAllConnections($step.id);
                    $step.hide();
                    $step.connectionPoint = connPoint;
                };
                for (var step in this.steps) {
                    $step = this.steps[step];
                    $step.connect();
                };

            } else {
                this.view.viewNode.find('#' + this.id).css({"display": "inline"});
                this.view.viewNode.find("#" + this.id).children().find("#content").css({"display": "inline"});
                if (this.collapsedFlag) {
                    for (var step in this.steps) {
                        $step = this.steps[step];
                        $step.connectionPoint = this.id;
                    };
                    for (var step in this.steps) {
                        $step = this.steps[step];
                        $step.connect();
                    };
                } else {
                    for (var step in this.steps) {
                        $step = this.steps[step];
                        $step.show();
                        $step.connectionPoint = $step.id;
                    };
                    for (var step in this.steps) {
                        $step = this.steps[step];
                        $step.connect();
                    };
                }
            }
            for (var d in this.daughters) {
                this.daughters[d].update_subcluster(visibleFlag, connPoint);
            }
        },
        collapse: function(){
            this.view.viewNode.find("#" + this.id).removeClass("cluster");
            this.view.viewNode.find("#" + this.id).addClass("clusterCollapsed");
            this.view.jsPlumb.detachAllConnections(this.id);
            // this.update_subcluster(false);
            for (var step in this.steps) {
                $step = this.steps[step];
                this.view.jsPlumb.detachAllConnections($step.id);
                $step.hide();
                $step.connectionPoint = this.id;
            };
            for (var step in this.steps) {
                $step = this.steps[step];
                $step.connect();
            }
            for (var d in this.daughters) {
                this.view.jsPlumb.detachAllConnections(this.daughters[d].id);
                this.daughters[d].update_subcluster(false, this.id);
            };
            this.view.viewNode.find("#" + this.id).find("#content").css({"display": "inline",});
            this.collapsedFlag = true;
        },
        expand: function(){
            this.view.viewNode.find("#" + this.id).addClass("cluster");
            this.view.viewNode.find("#" + this.id).removeClass("clusterCollapsed");
            this.view.jsPlumb.detachAllConnections(this.id);
            // this.update_subcluster(true);
            for (var step in this.steps) {
                $step = this.steps[step];
                $step.show();
                $step.connectionPoint = $step.id;
            };
            for (var step in this.steps) {
                $step = this.steps[step];
                $step.connect();
            }
            for (var d in this.daughters) {
                this.daughters[d].update_subcluster(true, this.id);
            };
            this.view.viewNode.find("#" + this.id).find("#content").css({"display": "none",});
            this.collapsedFlag = false;
        },
        toggle: function(){
            if (this.collapsedFlag) {
                this.expand();
            } else {
                this.collapse();
            };
            // console.log($("#" + this.id));
            // seen = [];
        },
        getSpan: function() {
            var content =  "<span id='content' style='display: none;'>" + this.name + "</span>"
            var $html = $("<span class='cluster' style='padding:2px; margin:2px; position:absolute; bottom: " + this.y + "px; left: " + this.x + "px; width: " + this.width + "px; height: " + this.height + "px;' id='" + this.id + "'>").html(content);
            // $html.draggable();
            return $html
        }

    });


    var AWMCenterView = CenterView._extend({

        init: function init(args) {
            init._super.apply(this, arguments);

            this.path = args.path;

            this.setupState({
                path: this.path
            }, args);

        },

        getFragment: function() {
            return this.getState("path") || "";
        },

        applyFragment: function(fragment) {
            this.setState("path", fragment);
            return this;
        },

        render: function($node) {
            var self = this;
            this.setLabel("AWM: " + self.path.replace(/^.*[\\\/]/, ''));             // Titel des "Tabs"

            this.getTemplate("html/tmpl.html", function(err, tmpl) {

                $node.html(tmpl);
                self.setLoading(true);

                var $jsPlumb = jsPlumb.getInstance();
                self.jsPlumb = $jsPlumb;
                self.viewNode = $node;

                var hideAllHighlights = function() {
                    steps.forEach(function(step) {
                        $step = stepDict[step];
                        $node.find("#" + $step.id).removeClass("highlighted");
                        $step.highlightedFlag = false;
                        $jsPlumb.select({source: $step.id}).setPaintStyle({strokeStyle: "black", lineWidth: 1,});
                    });
                }

                var $hideAllHighlightsBtn = $("<button class='btn btn-default' id='hideAllHighlightsBtn'>").html("Hide All Highlights");
                $hideAllHighlightsBtn.click(function(event) {
                    hideAllHighlights();
                });
                $node.find("#menubar").append($hideAllHighlightsBtn);
                $node.find("#menubar").append(" ");

                var $repaintConnectionsBtn = $("<button class='btn btn-default' id='repaintConnectionsBtn'>").html("Repaint Connections");
                $repaintConnectionsBtn.click(function(event) {
                    self.setLoading(true);
                    $jsPlumb.repaintEverything();
                    self.setLoading(false);
                });
                $node.find("#menubar").append($repaintConnectionsBtn);
                $node.find("#menubar").append(" ");

                var $expandAllClustersBtn = $("<button class='btn btn-default' id='expandAllClustersBtn'>").html("Expand all clusters");
                $expandAllClustersBtn.click(function(event) {
                    $node.find('#treeView').tree('discloseAll');
                });
                $node.find("#menubar").append($expandAllClustersBtn);
                $node.find("#menubar").append(" ");

                var $collapseAllClustersBtn = $("<button class='btn btn-default' id='collapseAllClustersBtn'>").html("Collapse all clusters");
                $collapseAllClustersBtn.click(function(event) {
                    $node.find('#treeView').tree('collapse');
                });
                $node.find("#menubar").append($collapseAllClustersBtn);
                $node.find("#menubar").append(" ");

                var $settingsBtn = $("<button class='btn btn-default' id='settingsBtn'>").html("Settings");
                $settingsBtn.click(function(event) {
                    var header = "<i class='fa fa-gears'></i> Analysis Workflow Management";
                    var body = "<h3>Settings</h3>" + "<table>" + "<tr><td>rankdir:&nbsp;</td><td> <select id='userSettings_rankdir'>";
                        switch (self.getPreference("rankdir")) {
                            case "TB":
                                body += "<option selected>TB</option>" +
                                    "<option>BT</option>" +
                                    "<option>LR</option>" +
                                    "<option>RL</option>";
                                break;
                            case "BT":
                                body += "<option>TB</option>" +
                                    "<option selected>BT</option>" +
                                    "<option>LR</option>" +
                                    "<option>RL</option>";
                                break;
                            case "LR":
                                body += "<option>TB</option>" +
                                    "<option>BT</option>" +
                                    "<option selected>LR</option>" +
                                    "<option>RL</option>";
                                break;
                            case "RL":
                                body += "<option>TB</option>" +
                                    "<option>BT</option>" +
                                    "<option>LR</option>" +
                                    "<option selected>RL</option>";
                                break;
                        };
                        body += "</select></td></tr><tr><td>ratio:&nbsp;</td><td><input id='userSettings_ratio' type='number' value='" +
                            self.getPreference("ratio") + "' step='0.01' min='0' max='5' width='4'></td></tr>" +
                            "<tr><td>engine:&nbsp;</td><td> <select id='userSettings_engine'>";
                        switch (self.getPreference("engine")) {
                            case "dot":
                                body += "<option selected>dot</option>" +
                                    "<option>twopi</option>" +
                                    "<option>circo</option>" +
                                    "<option>osage</option>";
                                break;
                            case "twopi":
                                body += "<option>dot</option>" +
                                    "<option selected>twopi</option>" +
                                    "<option>circo</option>" +
                                    "<option>osage</option>";
                                break;
                            case "circo":
                                body += "<option>dot</option>" +
                                    "<option>twopi</option>" +
                                    "<option selected>circo</option>" +
                                    "<option>osage</option>";
                                break;
                            case "osage":
                                body += "<option>dot</option>" +
                                    "<option>twopi</option>" +
                                    "<option>circo</option>" +
                                    "<option selected>osage</option>";
                                break;
                        }
                        body += "</select></td></tr></table>"
                        body += "get dot, get svg/pdf..., save svg/pdf..."
                    var $footer = $("<div><button class='btn btn-default' id='closeUserSettingsBtn'>Cancel</button>&nbsp;<button class='btn btn-primary' id='userSettingsApplyBtn'>Apply changes</button></div>");
                    self.dialog({
                        header: header,
                        body: body,
                        footer: $footer,
                        onRender: function() {
                            var settingsBox = this;
                            $footer.find("#closeUserSettingsBtn").click(function() {
                                settingsBox.close();
                            });
                            $footer.find("#userSettingsApplyBtn").click(function() {
                                // console.log($(settingsBox).$body.find("#userSettings_rankdir").val())
                                // console.log($(settingsBox).$body.find("#userSettings_ratio").val())
                                // console.log($(settingsBox).$body.find("#userSettings_engine").val())

                                self.setPreference("rankdir", $("#userSettings_rankdir").val());
                                self.setPreference("ratio", parseFloat($("#userSettings_ratio").val()));
                                self.setPreference("engine", $("#userSettings_engine").val());
                                // self.close();
                                settingsBox.close();
                            });
                        }
                    });
                });
                // $node.find("#menubar").append($settingsBtn);
                // $node.find("#menubar").append(" ");


                var $zoomBtnsGroup = $("<div class='btn-group btn-group-md'>").html("");
                var $decreaseZoomBtn = $("<button class='btn btn-default' id='decreaseZoomBtn'>").html("-");
                $decreaseZoomBtn.click(function(event) {
                    scaleFactor = scaleFactor/(1 + zoomFactor);
                    $node.find('#graphdiv').css({ transform: 'scale(' + scaleFactor + ')' });
                    $jsPlumb.setZoom(scaleFactor);
                });
                var $resetScaleFactorBtn = $("<button class='btn btn-default' id='resetScaleFactorBtn'>").html("reset zoom");
                $resetScaleFactorBtn.click(function(event) {
                    scaleFactor = 1;
                    $node.find('#graphdiv').css({ transform: 'scale(' + scaleFactor + ')' });
                    $jsPlumb.setZoom(scaleFactor);
                });
                var $increaseZoomBtn = $("<button class='btn btn-default' id='increaseZoomBtn'>").html("+");
                $increaseZoomBtn.click(function(event) {
                    scaleFactor = scaleFactor*(1 + zoomFactor);
                    $node.find('#graphdiv').css({ transform: 'scale(' + scaleFactor + ')' });
                    $jsPlumb.setZoom(scaleFactor);
                });
                $zoomBtnsGroup.append($decreaseZoomBtn);
                $zoomBtnsGroup.append($resetScaleFactorBtn);
                $zoomBtnsGroup.append($increaseZoomBtn);
                $node.find("#menubar").append($zoomBtnsGroup);
                // $node.find("#menubar").append(" ");

                // var $settingsBtn = $("<span id='settingsBtn'>").html("<a href='#' data-toggle='popover' title='Popover Header' data-content='Some content inside the popover'><i class='fa fa-gears'></i></a>");
                // $node.find("#menubar").append($settingsBtn);
                // $node.find("#menubar").append(" ");
                // $node.find("#menubar").append("<i class="fa fa-gears"></i>")
                var scaleFactor = 1;
                var zoomFactor = 0.03;
                var xLast = 0;
                var yLast = 0;
                var xGraph = 0;
                var yGraph = 0;
                $node.find('#graphdiv').bind('DOMMouseScroll', function(e){
                    // current location on screen
                    // var xScreen = e.originalEvent.pageX;
                     // - $(this).offset().left;
                    var xMouse = e.originalEvent.pageX;
                    var yMouse = e.originalEvent.pageY;
                    var currentScrollTop = $node.scrollTop();
                    var currentScrollLeft = $node.scrollLeft();
                    // var yScreen = e.originalEvent.pageY;
                     // - $(this).offset().top;
                    // console.log(e);
                    // console.log($(this).offset().top);
                    // current location on the graph at curretn scale
                    // xGraph = xGraph + ((xMouse - xLast) / scaleFactor);
                    // yGraph = yGraph + ((yMouse - yLast) / scaleFactor);
                    xGraph = (currentScrollLeft + xMouse)/scaleFactor;
                    yGraph = (currentScrollTop + yMouse)/scaleFactor;

                    if (e.originalEvent.detail > 0) {
                        scaleFactor = (1 + zoomFactor)*scaleFactor;
                    } else {
                        scaleFactor = scaleFactor/(1 + zoomFactor);
                    }

                    xGraphNew = (currentScrollLeft + xMouse)/scaleFactor;
                    yGraphNew = (currentScrollTop + yMouse)/scaleFactor;

                    // var xNew = (xMouse - xGraph) / scaleFactor;
                    // var yNew = (yMouse - yGraph) / scaleFactor;

                    // save the current screen location
                    // xLast = xMouse;
                    // yLast = yMouse;

                    var xShift = xGraph - xGraphNew;
                    var yShift = yGraph - yGraphNew;

                    // redraw
                    // $(this).find('div')
                    // $('#graphdiv').css('-moz-transform', 'scale(' + scaleFactor + ')' + 'translate(' + xNew + 'pt, ' + yNew + 'pt' + ')')
                    // $('#graphdiv').css('-moz-transform-origin', xGraph + 'pt ' + yGraph + 'pt')
                    // $('#graphdiv').css('-moz-transform-origin', 0 + 'pt ' + 0 + 'pt')
                    // console.log(xShift);

                    $node.find('#graphdiv').css({ transform: 'scale(' + scaleFactor + ')' });
                    $node.scrollTop(currentScrollTop + yShift);
                    $node.scrollLeft(currentScrollLeft + xShift);
                    $jsPlumb.setZoom(scaleFactor);
                    // console.log(scaleFactor);
                    //prevent page fom scrolling
                    return false;
                });

                // $("#graphdiv").mousewheel(function(e, delta) {
                //     // current location on screen
                //     var xScreen = e.pageX - $(this).offset().left;
                //     var yScreen = e.pageY - $(this).offset().top;
                //     // current location on the graph at curretn scale
                //     xGraph = xGraph + ((xScreen - xLast) / scaleFactor);
                //     yGraph = yGraph + ((yScreen - yLast) / scaleFactor);

                //     // determine new scaleFactor
                //     if (delta > 0) {
                //         scaleFactor = scaleFactor*(1+zoomFactor);
                //     } else {
                //         scaleFactor = scaleFactor/(1+zoomFactor);
                //     }
                //     // determine the location on the screen at the new scale
                //     var xNew = (xScreen - xGraph) / scaleFactor;
                //     var yNew = (yScreen - yGraph) / scaleFactor;

                //     // save the current screen location
                //     xLast = xScreen;
                //     yLast = yScreen;

                //     // redraw
                //     // $(this).find('div')
                //     $node.find('#graphdiv').css('-moz-transform', 'scale(' + scaleFactor + ')' + 'translate(' + xNew + 'px, ' + yNew + 'px' + ')')
                //                            .css('-moz-transform-origin', xGraph + 'px ' + yGraph + 'px')
                //     return false;
                // });



                $node.find('#graphdiv').bind('mousewheel', function(e){
                    if(e.originalEvent.wheelDelta < 0) {
                        //scroll down
                        scaleFactor = (1 + zoomFactor)*scaleFactor;
                        // console.log(scaleFactor);
                        $node.find('#graphdiv').css({ transform: 'scale(' + scaleFactor + ')' });
                        $jsPlumb.setZoom(scaleFactor);
                    } else {
                        //scroll up
                        scaleFactor = scaleFactor/(1 + zoomFactor);
                        // console.log(scaleFactor);
                        $node.find('#graphdiv').css({ transform: 'scale(' + scaleFactor + ')' });
                        $jsPlumb.setZoom(scaleFactor);
                    }
                    //prevent page fom scrolling
                    return false;
                });


                $node.find('#graphdiv').bind('mousedown', function(event) {
                    // console.log(event)
                    var startingTop = $node.scrollTop();
                    var startingLeft = $node.scrollLeft();
                    var mouseTop = event.offsetY;
                    var mouseLeft = event.offsetX;
                    document.onmousemove = function(e) {
                        e = e || event
                        var leftval = e.offsetX;
                        var topval = e.offsetY
                        $node.scrollTop(startingTop + mouseTop - topval);
                        $node.scrollLeft(startingLeft + mouseLeft - leftval);
                    }
                    document.onmouseup = function() {
                        document.onmousemove = null
                    }
                    return false;
                });

                // $('*').bind('mousedown', function(event) {
                //     console.log(event)
                // });

                $node.find("#treecontroller").click(function() {
                    $node.find("#opentree").toggle();
                    $node.find("#treecontent").toggle();
                    $node.find("#closetree").toggle();
                    $node.find("#wrapper").toggleClass("wrapperopen");
                    $node.find("#tree").toggleClass("treeopen");
                    $node.find("#wrapper").toggleClass("wrapperclosed");
                    $node.find("#tree").toggleClass("treeclosed");
                });
                $node.find("#mapcontroller").click(function() {
                    $node.find("#openmap").toggle();
                    $node.find("#mapcontent").toggle();
                    $node.find("#closemap").toggle();
                    // $node.find("#wrapper").toggleClass("wrapperopen");
                    $node.find("#mapdiv").toggleClass("mapopen");
                    // $node.find("#wrapper").toggleClass("wrapperclosed");
                    $node.find("#mapdiv").toggleClass("mapclosed");
                });
                $node.find("#infoBoxController").click(function() {
                    $node.find("#openInfoBox").toggle();
                    $node.find("#infoBoxContent").toggle();
                    $node.find("#closeInfoBox").toggle();
                    $node.find("#infoBox").toggleClass("infoBoxOpen");
                    $node.find("#infoBox").toggleClass("infoBoxClosed");
                    // $node.find("#infoBox").resizable({ handles: 'w, ' });
                });

                // self.GET("getvisualization", function(err, vis) {


                    var clusterList = [];
                    var whereIsStep = {};
                    var treeData = [];
                    var steps = [];
                    var whereIs = function(data) {
                        for (var i in data.steps) {
                            if (i in steps) {} else {steps.push(i)};
                            clusterList.push(data.name);
                            var temp = clusterList;
                            whereIsStep[i] = JSON.parse(JSON.stringify(temp));
                            clusterList.pop()
                        };
                        if (data.hasOwnProperty("daughters")) {
                            clusterList.push(data.name);
                            for (var j in data.daughters) {
                                whereIs(data.daughters[j]);
                            };
                            clusterList.pop();
                        };
                    };

                    var goToCluster = function(data, clusterList) {
                        for (var i = 0; i < clusterList.length; i++) {
                            data = data.daughters[clusterList[i]];
                        }
                        return data
                    }

                    var clusterList = [];
                    var clusterDict = {};
                    var stepDict = {};
                    self.stepDict = stepDict;
                    var $root = new Cluster(self)
                    $root.name = "root";
                    $root.id = "cluster_root";
                    // $tempCluster = $root;
                    clusterDict["graphdiv"] = $root;

                    var parentId = "graphdiv";
                    var createSpanStructure = function(data) {
                        if (data.name == "root") {
                            for (var i in data.steps) {
                                var $step = new Step(self);
                                $step.name = i;
                                $step.id = i + String((new Date()).getTime());
                                $step.connectionPoint = $step.id;
                                $step.childSteps = data.steps[i].childSteps;
                                for (var j in data.steps[i].requiredDataCollections) {
                                    for (var k in data.steps[i].requiredDataCollections[j]) {
                                        if ($.inArray(data.steps[i].requiredDataCollections[j][k].creatorName, $step.creatorSteps) != -1) {
                                        } else {
                                            $step.creatorSteps.push(data.steps[i].requiredDataCollections[j][k].creatorName);
                                        }
                                    }
                                }
                                $step.requiredDataCollections = data.steps[i].requiredDataCollections;
                                $root.steps.push($step);
                                stepDict[$step.name] = $step;
                            }
                        };
                        if (data.hasOwnProperty("daughters")) {
                            for (var d in data.daughters) {
                                clusterList.push(d);
                                // console.log(data.daughters[d]);
                                var $cluster = new Cluster(self);
                                var tempName = String(d);
                                $cluster.name = tempName;
                                var clusterId = "";
                                for (var i = 0; i < clusterList.length; i++) {
                                    clusterId += clusterList[i];
                                    if (i != clusterList.length-1) {parentId += clusterList[i];};
                                }
                                $cluster.id = "cluster_" + clusterId;
                                // console.log("data daughters [d]");
                                // console.log(data.daughters[d].steps);
                                for (var i in data.daughters[d].steps) {
                                    var $step = new Step(self);
                                    $step.name = data.daughters[d].steps[i].name;
                                    $step.id = i + String((new Date()).getTime());
                                    $step.connectionPoint = $step.id;
                                    $step.childSteps = data.daughters[d].steps[i].childSteps;
                                    for (var j in data.daughters[d].steps[i].requiredDataCollections) {
                                        for (var k in data.daughters[d].steps[i].requiredDataCollections[j]) {
                                            if ($.inArray(data.daughters[d].steps[i].requiredDataCollections[j][k].creatorName, $step.creatorSteps) != -1) {
                                            } else {
                                                $step.creatorSteps.push(data.daughters[d].steps[i].requiredDataCollections[j][k].creatorName);
                                            }
                                        }
                                    }
                                    $step.requiredDataCollections = data.daughters[d].steps[i].requiredDataCollections;
                                    $cluster.steps.push($step);
                                    stepDict[$step.name] = $step;
                                };
                                if (data.name == "root") {
                                    parentId = "graphdiv";
                                    $root.daughters[$cluster.name] = $cluster;
                                } else { }; // $tempCluster.daughters[$cluster.name] = $cluster; };
                                // console.log(clusterList);
                                // console.log(data.name);
                                clusterDict[$cluster.id] = $cluster;
                                $node.find("#" + parentId).append($cluster.getSpan());
                                clusterDict[parentId].daughters[$cluster.name] = $cluster;
                                // $node.find("#" + $cluster.id).append("<br>" + String(clusterList))
                                // console.log($cluster);
                                parentId = "cluster_";
                                if (data.daughters[d].hasOwnProperty("daughters")) {
                                    // var $oldTempCluster = $tempCluster;
                                    // $tempCluster = $cluster;
                                    // parentId = clusterId;
                                    createSpanStructure(data.daughters[d]);
                                    // clusterList.pop();
                                } else {
                                    // $tempCluster = $oldTempCluster;
                                    // parentId = "";
                                    // if (clusterList.length == 0) {parentId = "graphdiv"};
                                    // for (var i = 0; i < clusterList.length-1; i++) {
                                    //     parentId += clusterList[i];
                                    // };
                                };
                                clusterList.pop();
                            };
                        };
                    };


                    var positionSpans = function(data) {
                        var tempClusterList = [];
                        var getSVGCoordinatesOfSpans = function(data) {
                            tempClusterList.push(data.name);
                            var xMax = 0;
                            var yMax = 0;
                            var xMin = 10000000;
                            var yMin = -10000000;
                            var getInnerSVGCoords = function(data) {
                                for (var s in data.steps) {
                                    var tempXmax = data.steps[s].x + data.steps[s].width;
                                    var tempYmin = data.steps[s].y + data.steps[s].height;
                                    if (xMax < tempXmax) {xMax = tempXmax};
                                    if (yMax > data.steps[s].y) {yMax = data.steps[s].y};
                                    if (xMin > data.steps[s].x) {xMin = data.steps[s].x};
                                    if (yMin < tempYmin) {yMin = tempYmin};
                                };
                                for (var d in data.daughters) {
                                    getInnerSVGCoords(data.daughters[d]);
                                };
                            };
                            getInnerSVGCoords(data);
                            var clusterId = "cluster_"
                            // console.log(tempClusterList);
                            for (var i = 0; i < tempClusterList.length; i++) {
                                clusterId += tempClusterList[i];
                            }
                            if (data.name == "root") {clusterId = "graphdiv"};
                            // console.log(clusterId);
                            clusterDict[clusterId].svgx = xMin;
                            clusterDict[clusterId].svgy = yMax;
                            clusterDict[clusterId].width = xMax-xMin;
                            clusterDict[clusterId].height = yMin-yMax;
                            for (var d in data.daughters) {
                                getSVGCoordinatesOfSpans(data.daughters[d]);
                                tempClusterList.pop()
                            }
                        }

                        var getAbsoluteCoordinatesOfSpans = function(data) {
                            tempClusterList.push(data.name)
                            for (var d in data.daughters) {
                                getAbsoluteCoordinatesOfSpans(data.daughters[d]);
                                tempClusterList.pop();
                            };
                            var clusterId = "cluster_";
                            var parentId = "cluster_";
                            for (var i = 0; i < tempClusterList.length; i++) {
                                clusterId += tempClusterList[i];
                                if (i != tempClusterList.length-1) {parentId += tempClusterList[i]; };
                                if (tempClusterList.length == 1) {parentId =  "graphdiv"; };
                            }
                            // console.log(clusterId);
                            // console.log(parentId);
                            try {
                                clusterDict[clusterId].x = clusterDict[clusterId].svgx - clusterDict[parentId].svgx;
                                clusterDict[clusterId].y = clusterDict[clusterId].svgy - clusterDict[parentId].svgy;
                            } catch(err) {console.log(err);}
                            try {
                                $node.find("#" + clusterId).css({"left": clusterDict[clusterId].x,
                                    "bottom": clusterDict[clusterId].y,
                                    "width": clusterDict[clusterId].width,
                                    "height": clusterDict[clusterId].height});
                                // console.log("Positioned " + clusterId)
                            } catch (err) {
                                console.log("Error: clusterId: " + clusterId);
                                console.log(err);
                            }
                        };
                        for (var d in data.daughters) {
                            getSVGCoordinatesOfSpans(data.daughters[d]);
                            tempClusterList = [];
                            getAbsoluteCoordinatesOfSpans(data.daughters[d]);
                            tempClusterList = [];
                        }
                    };

                    var getClusterId = function(step) {
                        var clusterList = whereIsStep[step];
                        var clusterId = "";
                        for (var i = 1; i<clusterList.length; i++) {
                            clusterId += clusterList[i];
                        }
                        if (clusterId == "") {return "graphdiv"}
                        else {return "cluster_" + clusterId}
                    };

                    var steps = [];
                    var addStepsToSpanStructure = function(data) {
                        for (var step in data.steps) {
                            var $stepNode;
                            var style;
                            if ($.inArray(step, steps) != -1) { } else {steps.push(step);};
                            tempLeft = data.steps[step].x - clusterDict[getClusterId(step)].svgx;
                            tempBottom = data.steps[step].y - clusterDict[getClusterId(step)].svgy;
                            var $step = stepDict[step];
                            style = "position:absolute; bottom:" + tempBottom + "px; left:" + tempLeft + "px; width:" + data.steps[step].width + "px; text-align:center;";
                            $stepNode = $("<div class='step' id='" + $step.id + "' style='" + style + "'>").html(step);
                            // $stepNode.draggable();
                            // $stepNode.resizable();
                            // $jsPlumb.draggable($stepNode);
                            // console.log(style);
                            $node.find("#" + getClusterId(step)).append($stepNode);
                        };
                        if (data.hasOwnProperty("daughters")) {
                            for (var d in data.daughters) {
                                // console.log("Daughter")
                                // console.log(d)
                                addStepsToSpanStructure(data.daughters[d]);
                            };
                        };
                    };

                    var seen = [];
                    var connectAllSteps = function(data) {
                        for (var step in data.steps) {
                            $step = stepDict[data.steps[step].name];
                            $step.connect();
                        };
                        if (data.hasOwnProperty("daughters")) {
                            for (var d in data.daughters) {
                                connectAllSteps(data.daughters[d]);
                            };
                        };
                    };

                    var flagDot = function(flag) {
                        if (typeof flag == 'undefined') { return 'undefined'};
                        if (flag) {
                            html = '<i class="fa fa-check" style="color:green;"></i>'
                        } else {
                            html = '<i class="fa fa-times" style="color:red;"></i>'
                        }
                        return html
                    }

                    var whichUsage = function(inputCollection) {
                        if (inputCollection == "__config_set__") {
                            html = "<span class='label label-info' alt='__config_set__' data-toggle='tooltip' title='__config_set__'>Config</span>"
                        } else if(inputCollection == "__input_data_collection__"){
                            html = "<span class='label label-success' alt='__input_data_collection__' data-toggle='tooltip' title='__input_data_collection__'>Input</span>"
                        } else {
                            html = "<span class='label label-info' data-toggle='tooltip' title='" + inputCollection + "'>" + inputCollection + "</span>"
                        }
                        return html
                    }

                    $('[data-toggle="tooltip"]').tooltip();
                    var addInformationToSteps = function(data, steps) {
                        steps.forEach(function(step) {
                            var stepInfo = getStepInformation(data, step);
                            // console.log(stepInfo)
                            $stepObj = stepDict[step]
                            var $step = $node.find("#" + $stepObj.id);
                            $step.click(function($step) {
                                var alerttext = "<h4>" + step + "</h4>";
                                if (Object.keys(stepInfo.requiredDataCollections).length > 0) {
                                    alerttext += "<table style='width:100%'>";
                                    alerttext += "<tr><td colspan='4'><b>Required data collections</b></td></tr>";
                                    alerttext += "<tr><td colspan='2'>&nbsp;</td><td><i class='fa fa-clock-o' alt='allFlag' data-toggle='tooltip' title='allFlag'></i></td><td><i alt='mandatoryFlag' class='fa fa-exclamation-circle' data-toggle='tooltip' title='mandatoryFlag'></i></td></tr>";
                                    for (var i in stepInfo.requiredDataCollections) {
                                        alerttext += "<tr style='background-color:#E0E0E0;'><td colspan='4'>" + i + "</td></tr>";
                                        for (var j in stepInfo.requiredDataCollections[i]) {
                                            alerttext += "<tr style='background-color:#F0F0F0;'><td style='width:15px;'></td><td>" + stepInfo.requiredDataCollections[i][j].creatorName + "</td><td>" + flagDot(stepInfo.requiredDataCollections[i][j].allFlag) + "</td><td>" + flagDot(stepInfo.requiredDataCollections[i][j].mandatoryFlag) + "</td></tr>";
                                            alerttext += "<tr style='background-color:#F0F0F0; font-size:90%;'><td style='width:15px;'></td><td colspan='3'>" + stepInfo.requiredDataCollections[i][j].dataCollectionName + " " + whichUsage(stepInfo.requiredDataCollections[i][j].usage) + "</td></tr>";
                                        }
                                    }
                                    alerttext += "</table></br>"
                                }
                                var children = stepInfo.childSteps;
                                if (children.length > 0) {
                                    alerttext += "<b>Children</b><ul>";
                                    for (var i = 0; i < children.length; i++) {
                                        alerttext += "<li>" + children[i] + "</li>";
                                    }
                                    alerttext += "</ul></br>";
                                }
                                alerttext += "<b>Clusters</b><ul><li>(" + String(whereIsStep[step]).substring(5) + ")</li></ul><br><button class='btn btn-default' id='logfileBtn'>log files</button>&nbsp;<br>&nbsp;";
                                // self.alert(alerttext);
                                $node.find("#infoBoxContent").html(alerttext);
                                hideAllHighlights();
                                stepDict[step].highlightNearestFamily(stepDict[step].highlightedFlag, $node)
                                $node.find("#openInfoBox").css({"display": "none"});
                                $node.find("#infoBoxContent").css({"display": "inline"});
                                $node.find("#closeInfoBox").css({"display": "inline"});
                                $node.find("#infoBox").addClass("infoBoxOpen");
                                $node.find("#infoBox").removeClass("infoBoxClosed");
                                $node.find("#infoBox").scrollTop(0);
                                $node.find("#infoBox").scrollLeft(0);
                            });
                        });
                    };

                    var getStepInformation = function(data, step) {
                        clusterList = whereIsStep[step];
                        for (var i = 1; i < clusterList.length; i++) {
                            data = data.daughters[clusterList[i]]
                        }
                        return data.steps[step]
                    };

                    var getTreeBranch = function(data, options) {
                        try {
                            var parent = options.name;
                            // console.log(parent);
                            tempParentId = options.id.substr(4);
                            // console.log(tempParentId);
                        } catch(err) {
                            console.log(err);
                        }
                        if (typeof parent != 'undefined') {
                            data = data.daughters[parent];
                            data = clusterDict["cluster_" + tempParentId]
                            // console.log(data)
                        }
                        if (typeof tempParentId == 'undefined') {
                            tempParentId = "";
                        }
                        var treeList = [];
                        var someFlag = true;
                        var getInnerClusterTree = function(data) {
                            if (someFlag) {
                                if (data.hasOwnProperty("daughters")) {
                                    for (var d in data.daughters) {
                                    var cluster = {name: data.daughters[d].name, type: 'folder', id: "tree" + tempParentId + data.daughters[d].name,};
                                    treeList.push(cluster);
                                    };
                                };
                            };
                            someFlag = !someFlag;
                        };
                        getInnerClusterTree(data);
                        return treeList
                    }

                    self.GET("getmap", { filepath: self.path, engine: self.getPreference("engine") , rankdir: self.getPreference("rankdir") , ratio: self.getPreference("ratio") }, function(err, map) {
                        $node.find("#mapcontent").append(map)

                        $node.bind('scroll', function(event) {
                            var w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0)
                            var h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0)
                            // console.log(w);
                            // console.log(h);
                            var l = $node.scrollLeft();
                            var t = $node.scrollTop();
                            var viewWidth = window.innerWidth;
                            // console.log(viewWidth)
                            var nodeWidth = window.outerWidth;
                            // console.log(nodeWidth)
                            var h = $node.height();
                            // console.log(viewWidth/nodeWidth * 220);
                        })

                        var viewAreaTop = $node.find("#tree").offsetTop;
                        var viewAreaBottom = $node.find("#mapdiv").offsetTop;
                        var viewAreaWidth = $node.find("#menubar").width;
                        // console.log(viewAreaWidth)
                        // console.log(viewAreaBottom)
                        // console.log(viewAreaTop)
                    });

                    self.GET("getgraph", { filepath: self.path, engine: self.getPreference("engine") , rankdir: self.getPreference("rankdir") , ratio: self.getPreference("ratio") }, function(err, graph) {
                        $jsPlumb.ready(function() {
                            $jsPlumb.importDefaults({
                                    PaintStyle : {
                                    lineWidth: 1,
                                    strokeStyle: 'black'
                                },
                                DragOptions : { cursor: "crosshair" },
                                Endpoints : [ [ "Dot", { radius:4 } ], [ "Dot", { radius:4 } ] ],
                                EndpointStyles : [{ fillStyle:"black" }, { fillStyle:"black" }],
                                Anchor:[ "Perimeter", { shape:"Rectangle" } ],
                                Connector: ["Straight"],
                            });
                            // $jsPlumb.setContainer("graphdiv");

                            whereIs(graph);
                            createSpanStructure(graph);

                            positionSpans(graph);
                            addStepsToSpanStructure(graph);
                            connectAllSteps(graph);
                            addInformationToSteps(graph, steps);
                            // setTimeout(function(){connectAllSteps(graph);}, 50);
                            // setTimeout(function(){$jsPlumb.repaintEverything();}, 300);

                            $node.find('#treeView').tree({
                                dataSource: function (options, callback) {
                                    setTimeout(function () {
                                        // console.log(options);
                                        // console.log(callback);
                                        callback({data: getTreeBranch(graph, options)});
                                    }, 0);
                                },
                                cacheItems: true,
                                folderSelect: false,
                                multiSelect: false,
                                ignoreRedundantOpens: true
                            });

                            setTimeout(function() {$node.find('#treeView').tree('discloseAll');}, 100);
                            setTimeout(function() {
                                $node.find('#treeView').on('opened.fu.tree', function(e, data) {
                                    self.setLoading(true);
                                    var clusterId = "cluster_" + data.id.substr(4);
                                    var $cluster = clusterDict[clusterId];
                                    $cluster.expand($node);
                                    setTimeout(function(){$jsPlumb.repaintEverything();}, 150);
                                    self.setLoading(false);
                                });
                                $node.find('#treeView').on('closed.fu.tree', function(e, data) {
                                    self.setLoading(true);
                                    var clusterId = "cluster_" + data.id.substr(4);
                                    var $cluster = clusterDict[clusterId];
                                    $cluster.collapse($node);
                                    setTimeout(function(){$jsPlumb.repaintEverything();}, 150);
                                    self.setLoading(false);
                                });
                            }, 3000);
                            $jsPlumb.unbind("click");
                            $jsPlumb.unbind("mousewheel");
                            $jsPlumb.unbind("DOMMouseScroll");
                            // $('body').minimap();
                            // $node.minimap({
                                // heightRatio : 0.6,
                                // widthRatio : 0.05,
                                // offsetHeightRatio : 0.035,
                                // offsetWidthRatio : 0.035,
                                // position : "left",
                                // touch: true,
                                // smoothScroll: true,
                                // smoothScrollDelay: 200,
                                // onPreviewChange: function(minimap, scale) {}
                            // });
                            self.setLoading(false);
                         });
                    })
                // });
            });
        }
    });


    return AWMExtension;
});
