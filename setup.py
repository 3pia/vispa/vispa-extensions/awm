#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

setup(
    name             = "vispa_awm",
    version          = "0.1",
    description      = "VISPA AWM Extension - Inspect analyses designed with AWM.",
    author           = "VISPA Project",
    author_email     = "vispa@lists.rwth-aachen.de",
    url              = "http://vispa.physik.rwth-aachen.de/",
    license          = "GNU GPL v2",
    packages         = find_packages(),
    package_dir      = {"vispa_awm": "vispa_awm"},
    package_data     = {"vispa_awm": [
        "workspace/*",
        "static/css/*",
        "static/js/*",
        "static/js/vendor/*",
        "static/html/*"
    ]},
    # install_requires = ["vispa"],
)
